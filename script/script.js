// знаходимо всі кнопки з класом key
const keys = document.querySelectorAll('.btn');

// функція для зміни кольору кнопки
function highlightKey(event) {
  // знаходимо кнопку, яку потрібно змінити
  const key = document.querySelector(`button[data-key="${event.keyCode}"]`);
  
  // якщо така кнопка знайдена
  if (key) {
    // забираємо клас highlighted з усіх кнопок
    keys.forEach((key) => {
      key.classList.remove('highlighted');
    });
    // додаємо клас highlighted до поточної кнопки
    key.classList.add('highlighted');
  }
}

// слухач подій для натискання клавіш на клавіатурі
document.addEventListener('keydown', highlightKey);
